import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class testApiRest {
    @Test
    public void isAlive()
            throws IOException {

        // Given
        HttpUriRequest request = new HttpGet( "http://localhost:8080/rest/isAlive");

        // When
        HttpClientBuilder buildRequest = HttpClientBuilder.create();
        CloseableHttpResponse response = buildRequest.build().execute(request);
        // Then
        assertThat(
                response.getStatusLine().getStatusCode(),
                IsEqual.<Integer>equalTo(200));
    }
}
