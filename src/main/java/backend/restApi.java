package backend;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;

@Controller
@RequestMapping("/")

@CrossOrigin
public class restApi {

    File dowloadFile;
    String uploadsDir = "/uploads/";

    @RequestMapping(value="isAlive", method = RequestMethod.GET)
    public ResponseEntity isAlive(){
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, String> uploaderDeploiement(@RequestParam MultipartFile file, HttpServletRequest httpServletRequest) throws IOException {

        //String realPathtoUploads =  httpServletRequest.getServletContext().getRealPath(uploadsDir);
        String realPathtoUploads =  uploadsDir;
        if(! new File(realPathtoUploads).exists())
        {
            new File(realPathtoUploads).mkdir();
        }

        String filename = file.getOriginalFilename();
        String filePath = realPathtoUploads + filename;

        File newFile = new File(filePath);

        file.transferTo(newFile);

        dowloadFile = newFile;

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(newFile)));

        String line;
        System.out.println("*****lecture du fichier*****");
        System.out.println("nom du fichier: " + newFile.getName());

        while ((line = bufferedReader.readLine()) != null){
            System.out.println(line);
        }

        HashMap< String, String > messageResponse = new HashMap< String, String>();
        messageResponse.put("fileName", file.getOriginalFilename());
        return messageResponse;
    }

    @RequestMapping(value = "download", method = RequestMethod.GET)
    public void download(HttpServletResponse response) throws IOException {
        System.out.println("nom du fichier:" + dowloadFile.getName());
        InputStream inputStream = new FileInputStream(dowloadFile);

        response.addHeader("Content-Disposition", "attachment; filename="+dowloadFile.getName());
        response.addHeader("Content-Type", "application/zip");

        org.apache.commons.io.IOUtils.copy(inputStream, response.getOutputStream());
        response.getOutputStream().flush();

    }

}
